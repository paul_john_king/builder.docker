FROM "alpine:3.13.2" AS stage

	# Install Docker

	RUN ["apk", "add", \
		"ca-certificates=20191127-r5", \
		"libseccomp=2.5.1-r1", \
		"runc=1.0.0_rc93-r0", \
		"containerd=1.4.4-r0", \
		"libmnl=1.0.4-r1", \
		"libnftnl-libs=1.1.8-r0", \
		"iptables=1.8.6-r0", \
		"ip6tables=1.8.6-r0", \
		"tini-static=0.19.0-r0", \
		"device-mapper-libs=2.02.187-r1", \
		"docker-engine=20.10.3-r1", \
		"docker-cli=20.10.3-r1", \
		"docker=20.10.3-r1" \
	]

	# Install Git

	RUN ["apk", "add", \
		"ca-certificates=20191127-r5", \
		"brotli-libs=1.0.9-r3", \
		"nghttp2-libs=1.42.0-r1", \
		"libcurl=7.76.1-r0", \
		"expat=2.2.10-r1", \
		"pcre2=10.36-r0", \
		"git=2.30.2-r0" \
	]

	# Install Golang

	RUN ["apk", "add", \
		"libgcc=10.2.1_pre1-r3", \
		"libstdc++=10.2.1_pre1-r3", \
		"binutils=2.35.2-r1", \
		"libgomp=10.2.1_pre1-r3", \
		"libatomic=10.2.1_pre1-r3", \
		"libgphobos=10.2.1_pre1-r3", \
		"gmp=6.2.1-r0", \
		"isl22=0.22-r0", \
		"mpfr4=4.1.0-r0", \
		"mpc1=1.2.0-r0", \
		"gcc=10.2.1_pre1-r3", \
		"musl-dev=1.2.2-r0", \
		"go=1.15.12-r0" \
	]

FROM "scratch"
	COPY --from="stage" "/" "/"
	ENV GOCACHE="/tmp/.cache"
	WORKDIR "/work"
	CMD "/bin/sh"
